import java.awt.*;

public class Overlay extends Drawable {

    public Overlay() {
        super(0,0,0,0);
    }

    @Override
    public void paint(Graphics2D g2d) {
        g2d.setColor(Color.WHITE);
        g2d.setFont(new Font("Arial", Font.BOLD, 45));
        g2d.drawString("Space Wars", 540, 100);
    }

    @Override
    public void update() {

    }

}
