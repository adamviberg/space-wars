import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class Drawable {

    BufferedImage image;
    private int x, y, width, height;

    public Drawable(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public abstract void paint(Graphics2D g2d);

    public abstract void update();

}
